package com.camunda.fox.example.versicherung.service;

import java.math.BigDecimal;
import java.util.Map;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.camunda.fox.example.versicherung.entity.Angebot;

@Named
public class AngebotsService {
  
  @PersistenceContext
  private EntityManager em;
  
  @Inject @Named
  private Map<String, Object> processVariables;
  
  public void angebotErstellen() {
    Angebot angebot = new Angebot();
    angebot.setFirstname((String) processVariables.get("vorname"));
    angebot.setLastname((String) processVariables.get("lastname"));    
    // berechne Monatsbeitrag 
    // (Hey, vielleicht sogar mit JBoss Drools!)
    BigDecimal montasbeitrag = new BigDecimal(100);    
    angebot.setMonatsbeitrag(montasbeitrag);
    
    em.persist(angebot);  
    em.flush(); 
    processVariables.put("angebotsId", angebot.getId());
  }
  
  @Produces @Named
  public Angebot angebot() {
    return em.find(Angebot.class, processVariables.get("angebotsId"));
  }  
}
