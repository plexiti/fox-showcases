package com.camunda.fox.showcase.invoice.en;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.BasicAuthenticationManager;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;


/**
 * TODO: Allow two invoices per creditor on the same day
 */
@Named("svnService")
public class PDFToSVNDelegate implements JavaDelegate {

  private String svnUrl = "https://camunda-jakob/svn/archive/invoices/";
  private String svnUser = "demo";
  private String svnPwd = "demo";

  public void execute(DelegateExecution execution) throws Exception {
    // retrieve process variables
    byte[] file = (byte[]) execution.getVariable(ProcessConstants.VARIABLE_INVOICE);
    String creditor = (String) execution.getVariable("creditor");
    Date invoiceDate = (Date) execution.getVariable("invoice_date");

    // format correctly
    String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(invoiceDate);
    String fileName = creditor.replace(" ", "_") + "_" + formattedDate + ".pdf";
    String commitComment = "Freigabeprotokoll...";

    // and do SVN handling
    storeInSubversion(file, fileName, commitComment);
  }

  private void storeInSubversion(byte[] file, String fileName, String commitComment) throws SVNException {
    try {
      ISVNAuthenticationManager authManager = new BasicAuthenticationManager(svnUser, svnPwd);
      DAVRepositoryFactory.setup();
      SVNURL url = SVNURL.parseURIDecoded(svnUrl);
      SVNRepository repository = DAVRepositoryFactory.create(url, null);
      repository.setAuthenticationManager(authManager);

      // Get exact value of the latest (HEAD) revision.
      long latestRevision = repository.getLatestRevision();
      System.out.println("Repository latest revision (before committing): " + latestRevision);

      ISVNEditor editor = repository.getCommitEditor(commitComment, null /* locks */, true /* keepLocks */, null /* mediator */);

      editor.openRoot(-1);
      editor.addFile(fileName, null, -1);

      editor.applyTextDelta(fileName, null);

      SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
      String checksum = deltaGenerator.sendDelta(fileName, new ByteArrayInputStream(file), editor, true);

      editor.closeFile(fileName, checksum);
      editor.closeDir();
      editor.closeEdit();
    } catch (Exception ex) {
      throw new IllegalArgumentException("PDF could not be stored to SVN: " + ex.getMessage(), ex);
    }

  }

}
