package com.camunda.fox.showcase.invoice.en.servlet;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.io.IOUtils;


@WebServlet(value = "/processDiagram", loadOnStartup = 1)
public class ProcessDiagramServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Inject
  private RepositoryService repositoryService;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {    
    String processDefinitionId = request.getParameter("processDefinitionId");    
    if (processDefinitionId==null) {
    	String processDefinitionKey = request.getParameter("processDefinitionKey");
    	
    	ProcessDefinition pdef = repositoryService.createProcessDefinitionQuery().processDefinitionKey(processDefinitionKey).latestVersion().singleResult();
    	
    	processDefinitionId = pdef.getId();
    }
    
    InputStream processDiagram = repositoryService.getProcessDiagram(processDefinitionId);

    response.setContentType("image/png");
    response.getOutputStream().write(IOUtils.toByteArray(processDiagram));
  }
}