package com.camunda.fox.bean;

import javax.inject.Named;

@Named
public class StdOutLogService {

	public void log(String msg) {
		System.out.println("********************");
		System.out.println(msg);
		System.out.println("********************");
	}
}
