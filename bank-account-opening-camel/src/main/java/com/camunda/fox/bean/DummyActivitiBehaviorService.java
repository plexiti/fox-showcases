package com.camunda.fox.bean;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;

@Named
public class DummyActivitiBehaviorService implements ActivityBehavior {

	@Inject
	StdOutLogService logService;
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		logService.log("Hello from DummyActivitiBehaviorService!");		
	}
}
