package com.camunda.fox.process;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.activiti.engine.test.mock.Mocks;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.camunda.fox.bean.DummyActivitiBehaviorService;
import com.camunda.fox.bean.StdOutLogService;
import com.camunda.fox.process.stub.CamelBehaviourStub;

import static org.mockito.Mockito.*;

/**
 * 
 * @author Nils Preusker - nils.preusker@camunda.com
 *
 */
public class OpenAccountTest {

	@Rule
	public ActivitiRule activitiRule = new ActivitiRule();

	@Test
	@Deployment(resources="open-account.bpmn")
	public void testApprovedPath() throws Exception {
		// Get services
		RuntimeService runtimeService = activitiRule.getRuntimeService();
		TaskService taskService = activitiRule.getTaskService();
		HistoryService historyService = activitiRule.getHistoryService();
		
		// Prepare fake camel behavior
		CamelBehaviourStub camelBehaviour = new CamelBehaviourStub();
		
		/*
		 * Spy the CamelBehavior stub and add two mocked services in order to
		 * later verify that the services were actually called.
		 */
		CamelBehaviourStub spiedCamelBehavior = spy(camelBehaviour);
		Mocks.register("camel", spiedCamelBehavior);
				
		DummyActivitiBehaviorService dummyService = mock(DummyActivitiBehaviorService.class);
		Mocks.register("dummyActivitiBehaviorService", dummyService);
		
		StdOutLogService stdOutLogService = mock(StdOutLogService.class);
		Mocks.register("stdOutLogService", stdOutLogService);
		
		// Start process instance
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("open-account");
		
		runtimeService.signal(processInstance.getId());
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("approved", true);
		
		Task task = taskService.createTaskQuery().singleResult();
		taskService.complete(task.getId(), variables);
		
		assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
		
		List<HistoricActivityInstance> activityInstances = historyService.createHistoricActivityInstanceQuery().list();
		assertEquals(10, activityInstances.size());
		
		assertEquals("order_received", activityInstances.get(0).getActivityId());
		assertEquals("wait_for_postident", activityInstances.get(1).getActivityId());
		assertEquals("check_documents", activityInstances.get(2).getActivityId());
		assertEquals("documents_approved_gateway", activityInstances.get(3).getActivityId());
		assertEquals("beforeSetupAccount", activityInstances.get(4).getActivityId());		
		assertEquals("set_up_account", activityInstances.get(5).getActivityId());
		assertEquals("afterSetupAccount", activityInstances.get(6).getActivityId());
		assertEquals("merging_gateway_1", activityInstances.get(7).getActivityId());
		assertEquals("dummyServiceTask", activityInstances.get(8).getActivityId());
		assertEquals("order_processed", activityInstances.get(9).getActivityId());
		
		/*
		 * Verify that the services were actually called
		 */
		verify(stdOutLogService, times(1)).log("Before Setup Account");
		verify(stdOutLogService, times(1)).log("After Setup Account");
		ArgumentCaptor<ActivityExecution> execArgument = ArgumentCaptor.forClass(ActivityExecution.class);
		/*
		 * FIXME: IMHO, these tests should pass
		 */
		verify(dummyService, times(1)).execute(execArgument.capture());		
		verify(spiedCamelBehavior, times(1)).execute(execArgument.capture());
	}

	@Test
	@Deployment(resources="open-account.bpmn")
	public void testNonApprovedPath() throws Exception {
		// Get services
		RuntimeService runtimeService = activitiRule.getRuntimeService();
		TaskService taskService = activitiRule.getTaskService();
		HistoryService historyService = activitiRule.getHistoryService();
		
		// Prepare fake camel behavior
		CamelBehaviourStub camelBehaviour = new CamelBehaviourStub(); 
		Mocks.register("camel", camelBehaviour);
		
		// Start process instance
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("open-account");
		
		runtimeService.signal(processInstance.getId());
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("approved", false);
		
		Task task = taskService.createTaskQuery().singleResult();
		taskService.complete(task.getId(), variables);
		
		assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
		
		List<HistoricActivityInstance> activityInstances = historyService.createHistoricActivityInstanceQuery().list();
		assertEquals(8, activityInstances.size());
		
		assertEquals("order_received", activityInstances.get(0).getActivityId());
		assertEquals("wait_for_postident", activityInstances.get(1).getActivityId());
		assertEquals("check_documents", activityInstances.get(2).getActivityId());
		assertEquals("documents_approved_gateway", activityInstances.get(3).getActivityId());
		assertEquals("inform_customer", activityInstances.get(4).getActivityId());
		assertEquals("merging_gateway_1", activityInstances.get(5).getActivityId());
		assertEquals("dummyServiceTask", activityInstances.get(6).getActivityId());
		assertEquals("order_processed", activityInstances.get(7).getActivityId());
	}
	
}
