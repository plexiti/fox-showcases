package com.camunda.fox.process;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.activiti.engine.test.mock.Mocks;
import org.junit.Rule;
import org.junit.Test;

import com.camunda.fox.mule.MuleService;
import com.camunda.fox.process.stub.CamelBehaviourStub;
import static org.mockito.Mockito.*;

public class OpenAccountTest {

	@Rule
	public ActivitiRule activitiRule = new ActivitiRule();

	@Test
	@Deployment(resources="open-account.bpmn")
	public void testApprovedPath() throws Exception {
		// Get services
		RuntimeService runtimeService = activitiRule.getRuntimeService();
		TaskService taskService = activitiRule.getTaskService();
		HistoryService historyService = activitiRule.getHistoryService();
		
		// Prepare mock for muleService
		MuleService muleService = mock(MuleService.class);
		Mocks.register("mule", muleService);
		
		// Start process instance
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("open-account-mule");
		
		runtimeService.signal(processInstance.getId());
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("approved", true);
		
		Task task = taskService.createTaskQuery().singleResult();
		taskService.complete(task.getId(), variables);
		
		assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
		
		List<HistoricActivityInstance> activityInstances = historyService.createHistoricActivityInstanceQuery().list();
		assertEquals(7, activityInstances.size());
		
		assertEquals("order_received", activityInstances.get(0).getActivityId());
		assertEquals("wait_for_postident", activityInstances.get(1).getActivityId());
		assertEquals("check_documents", activityInstances.get(2).getActivityId());
		assertEquals("documents_approved_gateway", activityInstances.get(3).getActivityId());
		assertEquals("set_up_account", activityInstances.get(4).getActivityId());
		assertEquals("merging_gateway_1", activityInstances.get(5).getActivityId());
		assertEquals("order_processed", activityInstances.get(6).getActivityId());
	}

	@Test
	@Deployment(resources="open-account.bpmn")
	public void testNonApprovedPath() throws Exception {
		// Get services
		RuntimeService runtimeService = activitiRule.getRuntimeService();
		TaskService taskService = activitiRule.getTaskService();
		HistoryService historyService = activitiRule.getHistoryService();
		
		// Prepare fake camel behavior
		CamelBehaviourStub camelBehaviour = new CamelBehaviourStub(); 
		Mocks.register("camel", camelBehaviour);
		
		// Start process instance
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("open-account-mule");
		
		runtimeService.signal(processInstance.getId());
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("approved", false);
		
		Task task = taskService.createTaskQuery().singleResult();
		taskService.complete(task.getId(), variables);
		
		assertEquals(0, runtimeService.createProcessInstanceQuery().list().size());
		
		List<HistoricActivityInstance> activityInstances = historyService.createHistoricActivityInstanceQuery().list();
		assertEquals(7, activityInstances.size());
		
		assertEquals("order_received", activityInstances.get(0).getActivityId());
		assertEquals("wait_for_postident", activityInstances.get(1).getActivityId());
		assertEquals("check_documents", activityInstances.get(2).getActivityId());
		assertEquals("documents_approved_gateway", activityInstances.get(3).getActivityId());
		assertEquals("inform_customer", activityInstances.get(4).getActivityId());
		assertEquals("merging_gateway_1", activityInstances.get(5).getActivityId());
		assertEquals("order_processed", activityInstances.get(6).getActivityId());
	}
	
}
